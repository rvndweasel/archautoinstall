#!/bin/sh
sed -i \
         -e 's/#202020/rgb(0%,0%,0%)/g' \
         -e 's/#ffffff/rgb(100%,100%,100%)/g' \
    -e 's/#1c1c1c/rgb(50%,0%,0%)/g' \
     -e 's/#4cbe99/rgb(0%,50%,0%)/g' \
     -e 's/#1c1c1c/rgb(50%,0%,50%)/g' \
     -e 's/#ffffff/rgb(0%,0%,50%)/g' \
	"$@"
