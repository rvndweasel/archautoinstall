#   _               _              
#  | |__   __ _ ___| |__  _ __ ___ 
#  | '_ \ / _` / __| '_ \| '__/ __|
#  | |_) | (_| \__ \ | | | | | (__ 
#  |_.__/ \__,_|___/_| |_|_|  \___|

#show system info when run konsole
neofetch --block_range 0 7 --cpu_brand off --cpu_cores off --cpu_speed off --ascii_distro linux --ascii_colors 0 4 3 --colors 4 4 4 4

# disable ctrl-s and ctrl-q
stty -ixon

# allows you to cd into directory merely by typing the directory name
shopt -s autocd

# load script for custom bash prompt
. ~/.bash_prompt.sh

# avoid duplicate commands being shown in history
export HISTCONTROL=ignoreboth:erasedups

# change bash history size
HISTSIZE=10000
HISTFILESIZE=10000

# aliases
    # custom
        alias neofetch='neofetch --block_range 0 7 --cpu_brand off --cpu_cores off --cpu_speed off --ascii_distro linux --ascii_colors 0 4 3 --colors 4 4 4 4'

        #alias ipaddress='ip route get 1.2.3.4 | awk '{print $7}''
        alias pushToSite='~/Documents/csPersonal/encoding/encoding\ tutorial\ project/webpage/pushSite.sh'
        alias encoding='cd ~/Documents/csPersonal/encoding/'   
        alias reboot='sudo /sbin/reboot'
        alias shutdown='sudo /sbin/shutdown'

        alias psmem='ps auxf | sort -nr -k 4'
        alias psmem10='ps auxf | sort -nr -k 4 | head -10'
        alias pscpu='ps auxf | sort -nr -k 3'
        alias pscpu10='ps auxf | sort -nr -k 3 | head -10'
        
    # rsync commands for laptop
        alias push='rsync -hruv --progress --exclude '.directory' ~/Documents/ 192.168.1.92:~/Documents/'
        alias pull='rsync -hruv --progress --exclude '.directory' --exclude 'books' 192.168.1.92:~/Documents/ ~/Documents/'

    # apt
        alias install='sudo apt install'
        alias update='sudo apt update -y && sudo apt full-upgrade -y && sudo apt autoremove -y && sudo apt clean -y && sudo apt autoclean -y'
        alias uninstall='sudo apt purge -y'

    # system
        alias ls='ls -ilh --color=auto'
        alias ll='ls -ilha --color=auto'

        alias grep='grep --color=auto'
        alias egrep='egrep --color=auto'
        alias fgrep='fgrep --color=auto'

        alias mv='mv -v'
        alias cp='cp -v'
        alias rm='rm -v'
        
		alias ccat='bat'
		
    # school
        alias fox01='ssh -X zpx669@fox01.cs.utsa.edu'
        alias fox02='ssh -X zpx669@fox02.cs.utsa.edu'

    # extract
        function extract {
            if [ -z "$1" ]; then
                # display usage if no parameters given
                echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
            else
                if [ -f $1 ] ; then
                # NAME=${1%.*}
                # mkdir $NAME && cd $NAME
                case $1 in
                    *.tar.bz2)   tar xvjf ./$1    ;;
                    *.tar.gz)    tar xvzf ./$1    ;;
                    *.tar.xz)    tar xvJf ./$1    ;;
                    *.lzma)      unlzma ./$1      ;;
                    *.bz2)       bunzip2 ./$1     ;;
                    *.rar)       unrar x -ad ./$1 ;;
                    *.gz)        gunzip ./$1      ;;
                    *.tar)       tar xvf ./$1     ;;
                    *.tbz2)      tar xvjf ./$1    ;;
                    *.tgz)       tar xvzf ./$1    ;;
                    *.zip)       unzip ./$1       ;;
                    *.Z)         uncompress ./$1  ;;
                    *.7z)        7z x ./$1        ;;
                    *.xz)        unxz ./$1        ;;
                    *.exe)       cabextract ./$1  ;;
                    *)           echo "extract: '$1' - unknown archive method" ;;
                esac
            else
                echo "$1 - file does not exist"
            fi
            fi
        }
        

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/weasel/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/weasel/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/weasel/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/weasel/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


