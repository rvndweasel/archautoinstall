"         _                    
"  __   _(_)_ __ ___  _ __ ___ 
"  \ \ / / | '_ ` _ \| '__/ __|
"   \ V /| | | | | | | | | (__ 
"    \_/ |_|_| |_| |_|_|  \___|


"########## GENERAL ##########"
""" LEADER KEY -----------------------
let mapleader=" "               " leader is comma


""" SPACES & TABS --------------------
set tabstop=4		            " number of visual spaces per tab
set softtabstop=4	            " number of actual spaces per tab 
set shiftwidth=4                " number of spaces to use for each step of (auto)indent
set expandtab		            " tabs are just expanded spaces
set autoindent		            " new lines inherit indentation of previous lines
set smartindent		            " smart version of autoindent used with autoindent


""" UI CONFIG ------------------------
syntax enable                   " enable syntax
syntax on                       " turn on syntax
set number relativenumber	    " show number lines
set showcmd			            " show last command ran in bottom bar
set cursorline		            " highlight the current line
set showmatch		            " highlight matching [{()}]
filetype plugin on              " load filetype-specific indent files
set termguicolors               " allow for gui colorschemes
set background=light            " put background as light
set t_ut=                       " deletes any unwanted highlighting


""" COLORSCHEME ----------------------
colorscheme molokai             " set colorscheme


"########## PLUGINS ##########"
""" AUTOINSTALL VIM-PLUG -------------
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


""" AUTOINSTALL PLUGINS --------------
autocmd! VimEnter * 
    \ if len(filter(values(g:plugs), '!isdirectory(v:val.dir)')) 
    \ | PlugInstall --sync | q | endif


""" VIM PLUGINS ----------------------
call plug#begin('~/.vim/plugged')  

Plug 'vim-syntastic/syntastic'  " syntax errorchecking
Plug 'preservim/nerdtree'       " file system explorer
Plug 'majutsushi/tagbar'        " browse tags of current file
Plug 'preservim/nerdcommenter'  " powerful commenter
Plug 'itchyny/lightline.vim'    " configurable statusline
Plug 'junegunn/fzf.vim'         " fuzzy finder
Plug 'yggdroot/indentline'      " visual indentation levels

call plug#end()


""" MAPPINGS -------------------------
map <C-n> :NERDTreeToggle<CR>
nmap <C-x> :TagbarOpenAutoClose<CR>
vnoremap <C-c> "+y
map <C-v> "+p
autocmd FileType python map <buffer> <F9> :w<CR>:exec '!python3' shellescape(@%, 1)<CR>

" close if nerdtree is last window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


""" LIGHTLINE SETTINGS ---------------
set laststatus=2

if !has('gui_running')
  set t_Co=256
endif

