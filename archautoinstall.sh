#!/usr/bin/env bash

echo -e "\n\n-------------------------------------------------"
echo -e "                    PREREQUISITES                    "
echo -e "-------------------------------------------------\n\n"
sleep 2

# Install correct microcode
proc_type=$(lscpu | awk '/Vendor ID:/ {print $3}')
case "$proc_type" in
	GenuineIntel)
		echo "Installing Intel microcode"
		sudo pacman -S --noconfirm --needed intel-ucode
		proc_ucode=intel-ucode.img
		;;
	AuthenticAMD)
		echo "Installing AMD microcode"
		sudo pacman -S --noconfirm --needed amd-ucode
		proc_ucode=amd-ucode.img
		;;
esac	

# Pacman Config File
echo "Editing /etc/pacman.conf"
sudo sed -i 's/^#Color/Color/' /etc/pacman.conf
sudo sed -i 's/^#Para/Para/' /etc/pacman.conf
sudo sed -i '/^Para/!b;n;cILoveCandy\n' /etc/pacman.conf
sudo sed -i 's/^#\[multilib\]/\[multilib\]/' /etc/pacman.conf
sudo sed -i '/^\[multilib\]/!b;n;cInclude = /etc/pacman.d/mirrorlist' /etc/pacman.conf
sudo pacman -Syy


# Get kernel version for linux headers
#number=$(uname -r | awk -F '.' '{print $1$2}')
#headers="linux$number"


echo -e "\n\n-------------------------------------------------"
echo -e "                INSTALLING PACKAGES                  "
echo -e "-------------------------------------------------\n\n"
sleep 2

PKGS=(
    # WM/DE ----------------------------------------
		# Graphical Server
		'xorg'							# X Window System display server
		'mesa' 							# An open-source implementation of the OpenGL specification

        # Session Manager
        'lightdm'                       # A lightweight display manager
        'lightdm-gtk-greeter'           # GTK+ greeter for LightDM

		# WM Core
		'i3-gaps'                       # A fork of i3wm tiling window manager with more features
		'picom'                         # X compositor that may fix tearing issues

		# WM Programs
		'polybar'                       # A fast and easy-to-use status bar
		'rofi'                          # A window switcher, application launcher and dmenu replacement

		# Notifications
		'dunst'                         # Customizable and lightweight notification-daemon

		# System Settings
		'xfce4-power-manager'      		# Power manager for Xfce desktop
		'xfce4-session'            		# Session manager for Xfce
		'xfce4-settings'           		# Settings manager for xfce
		'xfconf'                   		# A simple client-server configuration storage and query system
		'xfdesktop'                		# A desktop manager for Xfce

	# Network ----------------------------------------
		'networkmanager'                # Network connection manager and user applications
		'network-manager-applet'        # Applet for managing network connections
		'nm-connection-editor'          # NetworkManager GUI connection editor and widgets
		'wpa_supplicant'                # A utility providing key negotiation for WPA wireless networks
		'dhclient'                      # A standalone DHCP client from the dhcp package


	# Audio ----------------------------------------
		# Alsa
		'alsa-plugins'                  # Additional ALSA plugins
		'alsa-utils'                    # Advanced Linux Sound Architecture - Utilities
		'alsa-card-profiles'            # Low-latency audio/video router and processor - ALSA card profiles

		# Pulseaudio 
		'pulseaudio'                    # A featureful, general-purpose sound server
		'pulseaudio-alsa'               # ALSA Configuration for PulseAudio
		'pulseaudio-bluetooth'          # Bluetooth support for PulseAudio
		'pulseaudio-ctl'                # Control pulseaudio volume from shell or keyboard shortcuts
		'pulseaudio-jack'               # Jack support for PulseAudio
		'pulseaudio-lirc'               # IR (lirc) support for PulseAudio
		'pulseaudio-rtp'                # RTP and RAOP support for PulseAudio
		'pulseaudio-zeroconf'           # Zeroconf support for PulseAudio

		# GUI
		'pavucontrol'                   # PulseAudio Volume Control (GUI)
		'pulseaudio-equalizer'          # Graphical equalizer for PulseAudio
		'pulseaudio-equalizer-ladspa'   # A 15-band equalizer for PulseAudio


	# Bluetooth ----------------------------------------
		'bluez'                         # Daemons for the bluetooth protocol stack
		'bluez-utils'                   # Development and debugging utilities for bluetooth
		'blueman'                       # GTK+ Bluetooth Manager


	# Printing ----------------------------------------
		# Base Printing Install
		'cups'                          # The CUPS Printing System - daemon package
		'cups-filters'                  # OpenPrinting CUPS Filters
		'cups-pdf'                      # PDF printer for cups

		# Some printers need this interpreter
		'ghostscript'                   # An interpreter for the PostScript language
		'gsfonts'                       # (URW)++ base 35 font set

		# Drivers for Canon, Epson, Lexmark, Sony, Olympus, and PCL printers
		'gutenprint'                    # Top quality printer drivers for POSIX systems
		'foomatic-db-gutenprint-ppds'   # Simplified prebuilt ppd files

		# Config tools
		'cups-pk-helper'                # A helper that makes system-config-printer use PolicyKit
		'system-config-printer'         # A CUPS printer configuration tool and status applet



	# Programming ----------------------------------------
		# Code Text Editor
		'atom'                          # Text Editor for code
		'apm'                           # Atom package manager

		# C Family
		'gcc'                           # The GNU Compiler Collection - C and C++ frontends
		'clang'                         # C language family frontend for LLVM
		'sqlite'                        # A C library that implements an SQL database engine
		'c++utilities'                  # C++ classes and routines

		# Python
		'python'                        # Next generation of the python high-level scripting language
		'python-attrs'                  # Attributes without boilerplate.
		'python-beautifulsoup4'         # A Python HTML/XML parser designed for quick projects
		'python-lxml'                   # Python3 binding for the libxml2 and libxslt libraries
		'python-markdown'               # Python implementation of John Gruber's Markdown.
		'python-pip'                    # The PyPA recommended tool for installing Python packages
		'python-regex'                  # Alternative python regular expression module.
		'python-requests'               # Python HTTP for Humans

		# Java
		'jre-openjdk'                   # OpenJDK Java 16 full runtime environment
		'jdk-openjdk'                   # OpenJDK Java 16 development kit

		# Javascript
		'nodejs'                        # Evented I/O for V8 javascript
		'npm'                           # A package manager for javascript

		# Other Languages
		'clisp'                         # ANSI Common Lisp interpreter, compiler and debugger
		'lua'                           # Lightweight programming language designed for extending apps

		# Utilities
		'autoconf'                      # A GNU tool for automatically configuring source code
		'automake'                      # A GNU tool for automatically creating Makefiles
		'cmake'                         # A cross-platform open-source make system
		'make'                          # GNU make utility to maintain groups of programs
		'ninja'                         # Small build system with a focus on speed


	# Media ----------------------------------------
		# Video
		'mpv'                           # a free, open source, and cross-platform media player
		'celluloid'                     # Simple GTK+ frontend for mpv
		'vlc'                           # Multi-platform MPEG, VCD/DVD, and DivX player

		# Photo
		'eog'                           # Eye of Gnome: An image viewing and cataloging program
		'gimp'                          # GNU Image Manipulation Program
		'eog-plugins'                   # Plugins for Eye of Gnome
		'gimp-plugin-gmic'              # Gimp plugin for the G'MIC image processing framework

		# Document
		'evince'                        # Document viewer
		'foliate'                       # A simple and modern GTK eBook reader
		'mcomix'                        # Image viewer specifically designed to handle comic books
		'gedit'                         # GNOME Text Editor
		'gedit-plugins'                 # Collection of plugins for the gedit Text Editor
		'vim'                           # Vi Improved, a highly configurable version of the vi text editor

		# Music
		'gnome-music'                   # Music player and management application


	# Utilities ----------------------------------------
		# Web Browser
		'chromium'                      # A web browser built for speed, simplicity, and security
		'google-chrome'                 # The popular and trusted web browser by Google (Stable Channel)

		# Terminal
		'kitty'                         # A modern, hackable, featureful, OpenGL-based terminal emulator
		'kitty-terminfo'                # Terminfo for kitty, an OpenGL-based terminal emulator

    	# File Manager
		'thunar'                   		# Modern file manager for Xfce
		'thunar-archive-plugin'    		# Create and extract archives in Thunar
		'thunar-shares-plugin'     		# Share a folder using Samba without requiring root access in Thunar
		'thunar-volman'            		# Automatic management of removeable devices in Thunar
		'tumbler'                       # D-Bus service for applications to request thumbnails
		'ffmpegthumbnailer'             # Lightweight video thumbnailer that can be used by file managers.

		# Other
		'discord-canary'                # All-in-one voice and text chat for gamers
		'qalculate-gtk'                 # GTK frontend for libqalculate
		'transmission-gtk'              # Fast, easy, and free BitTorrent client (GTK+ GUI)
		'mediainfo-gui'                 # Supplies technical and tag information about a video or audio file
		
	# Gaming ----------------------------------------
		'steam'                         # Valve's digital software delivery system
		'vbam-wx'                       # Nintendo GameBoy Advance emulator
		'melon-ds'                      # DS emulator, sorta


	# Packages ----------------------------------------
		'bash'                          # The GNU Bourne Again shell
		'bash-completion'               # Programmable completion for the bash shell
		'feh'                           # Fast and light imlib2-based image viewer
		'ffmpeg'                        # Complete solution to record, convert and stream audio and video
		'flatpak'                       # Linux application sandboxing and distribution framework
		'htop'                          # Interactive process viewer
		'linux-api-headers'		        # Kernel headers sanitized for use in userspace
		'neofetch'                      # A CLI system information tool written in BASH
		'ntfs-3g'                       # NTFS filesystem driver and utilities
		'openssh'                       # Premier connectivity tool for remote login with the SSH protocol
		'rsync'                         # A fast and versatile file copying tool for remote and local files
		'cbonsai-git'                   # A bonsai tree generator, written in C using ncurses (git version)


	# Archiving ----------------------------------------
		'bzip2'                         # A high-quality data compression program
		'gzip'                          # GNU compression utility
		'unrar'                         # The RAR uncompression program
		'unzip'                         # For extracting and viewing files in .zip archives
		'zip'                           # Compressor/archiver for creating and modifying zipfiles


	# Codecs ----------------------------------------
		'opus'                          # Totally open, royalty-free, highly versatile audio codec
		'libopusenc'                    # High-level API for encoding .opus files
		'opus-tools'                    # Collection of tools for Opus audio codec
		'opusfile'                      # Library for opening, seeking, and decoding .opus files
		'x264'                          # Open Source H264/AVC video encoder
		'x265'                          # Open Source H265/HEVC video encoder
)

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    yay -S "$PKG" --noconfirm --needed
done


echo -e "\n\n-------------------------------------------------"
echo -e "                   SETUP SERVICES                    "
echo -e "-------------------------------------------------\n\n"
sleep 2

sudo systemctl enable bluetooth.service
sudo systemctl enable avahi-daemon.service
sudo systemctl enable lightdm.service 
sudo systemctl enable NetworkManager.service
sudo systemctl enable wpa_supplicant.service
sudo systemctl enable cups.service
sudo systemctl enable sshd.service
# xfce settings? 


echo -e "\n\n-------------------------------------------------"
echo -e "                    CONFIG FILES                     "
echo -e "-------------------------------------------------\n\n"
sleep 2

# Bash
cp dotfiles/.bash_profile ~/
cp dotfiles/.bash_prompt.sh ~/
cp dotfiles/.bashrc ~/

# Vim
cp -R dotfiles/.vim/ ~/.vim/
cp dotfiles/.vimrc ~/

# Theming
cp -R dotfiles/.icons/ ~/
cp -R dotfiles/.themes/ ~/
cp -R dotfiles/startpage/ ~/

# General Config
cp -R dotfiles/.config/ ~/
cp -R dotfiles/.local/bin/ ~/
cp -R dotfiles/.local/lib/ ~/
cp -R dotfiles/.local/share/ ~/

