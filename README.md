# ArchAutoInstall

## Description
ArchAutoInstall, inspired by ArchMatic, is a project with the goal of automating the process of installing an Arch-based linux distrobution. 

This script will do the following:
1. Install the correct microcode for your specific CPU (AMD/Intel)
2. Install all neccesary packages for a lightweight system
3. Make sure necessary services are enabled.
4. Copy over dotfiles


## Installation and Execution
Once you have your core system installed, the next step is to install all necessary packages and setup a few necessary things. In order to download this repo, you must first have git installed.

```pacman -S --noconfirm --needed git```  

Then you can download my repo.

```git clone https://gitlab.com/rvndweasel/archautoinstall.git && cd archautoinstall```  

Run the script.

```./archautoinstall.sh```
